/*

Simple Watch based on RTC to explore LCD and SPI in asm

Hardware: 
- Atmega328p 3Mhz (does not really matter)
- DeadOnRTC from sparkfun, based on DS3234 chip
- HD44780 compatible LCD display, 16x2

PINS:

- PORTD 8 data lines go to LCD
- PORTC 0,1,2 are used for LCD control lines
- PORTB 5,4,3,2 are used for SPI interface
  ** careful we get bus contention with the ISP line! Especially I have seen
  that the DS3234 gets reprogrammed while loading up the program!! I pulled
  SS pin to avoid that. I think resistors could fix that, not tested.


Software:

I poll the DS3234 in a tight loop, update the LCD all the time. Some hard coded
code to set the watch is in there but skipped

Note about the LCD: it really needs the 2ms delay, especially the clear screen
command. That did not work at all with anything faster. It would be better to
program that with checking the busy flag.

SPI is also suboptimially programmed, I get a single register in each cycle.
I should probably use multi byte access. But it works this way.

Note that I initially could not get SPI to work as I was setting SS high after
sending the command byte. This screwed the SPI totally up.
*/

.include "m328pdef.inc"

.equ FCPU = 3000000              ; 5 Mhz atmega328

; *** 8 BIT LCD INTERFACE -------------------------
.equ LCD_DATA     = PORTD
.equ LCD_DATA_DIR = DDRD
.equ LCD_CTL      = PORTC
.equ LCD_CTL_DIR  = DDRC
.equ LCD_RS       = 0
.equ LCD_RW       = 1
.equ LCD_E        = 2

; SPI interface
.equ SPI_MISO     = 4
.equ SPI_MOSI     = 3
.equ SPI_SCK      = 5
.equ SPI_SS       = 2

.cseg ;------------  FLASH ----------------------------------
.org 0x0000
                    rjmp Reset

; ------------------ DELAY SUBS ------------------------------
; *** we really need 2ms delays!
Delay:              push r16
                    ldi r16, 6
DelayLoop:          rcall Delay300ms
                    dec r16
                    brne DelayLoop
                    pop r16
                    ret

; --------  300 ms delay
Delay300ms:         push r17
                    ldi r17, 0xFF  ; 
Delay300Loop:          nop
                    dec r17
                    brne Delay300Loop
                    pop r17
                    ret

LongWait:           push r16
                    push r17
                    push r18
                    ldi r18, 0x20
LW1:                ldi r17, 0xFF
LW2:                ldi r16, 0xFF
LW3:                nop
                    dec r16
                    brne LW3
                    dec r17
                    brne LW2
                    dec r18
                    brne LW1
                    pop r18
                    pop r17
                    pop r16
                    ret

; ------------------- 8 Bit LCD routines ----------------------------------------

Lcd8BitCommand:     ; write a 8 bit command given in r16
                    cbi LCD_CTL, LCD_RS ; R/S = 0 -> command
                    out LCD_DATA, r16
                    sbi LCD_CTL, LCD_E ; E = 1 -> now strobe it
                    rcall Delay
                    cbi LCD_CTL, LCD_E ; E = 0 -> cmd ends
                    rcall Delay
                    ret

Lcd8BitData:        ; write a 8 bit data given in r16
                    sbi LCD_CTL, LCD_RS ; R/S = 1 -> Data
                    out LCD_DATA, r16
                    sbi LCD_CTL, LCD_E ; E = 1 -> now strobe it
                    rcall Delay
                    cbi LCD_CTL, LCD_E ; E = 0 -> cmd ends
                    rcall Delay
                    ret


LcdInit:            ; 8 bit lcd init blinking cursor
                    ; init 8 bit mode
                    rcall Delay

                    ldi r16, 0xFF
                    out LCD_DATA_DIR, r16
                    sbi LCD_CTL_DIR, LCD_RS
                    sbi LCD_CTL_DIR, LCD_RW
                    sbi LCD_CTL_DIR, LCD_E

                    ; control lines to zero
                    cbi LCD_CTL, LCD_RS
                    cbi LCD_CTL, LCD_RW
                    cbi LCD_CTL, LCD_E

                    ldi r16, 0x38
                    rcall Lcd8BitCommand

                    ; now cursor
                    ldi r16, 0x0f
                    rcall Lcd8BitCommand

                    ; now home
                    ldi r16, 0x02
                    rcall Lcd8BitCommand
                    ret


LcdString:          ; put out string in flash, pointed to by ZH/ZL 
                    lpm r16, Z+
                    cpi r16, 0
                    breq LcdStringExit
                    rcall Lcd8BitData
                    rjmp LcdString
LcdStringExit:      ret

LcdSetXY:           ; r16: bit 7 = 1-> line 1, bit 7 = 0 -> line 0
                    ; the rest is column
                    ; DDRAM for 16x2 -> line 0 has memory 0... line 1 has memory 0x40....
                    sbrc r16, 7  ; if it is  line 0 skip next instruction
                    ori r16, 0x40 ; add offset for line 1
                    ori r16, 0x80 ; always set command bit 7
                    rcall Lcd8BitCommand
                    ret

LcdClearScreen:     ; just clear screen
                    ldi r16, 0x01
                    rcall Lcd8BitCommand
                    ret

LcdHome:            ldi r16, 0x02 ; move cursor to home position
                    rcall Lcd8BitCommand
                    ret

LcdBCDOut:          ; r16 contains a BCD number, print it out as 2 digit decimal
                    push r16
                    swap r16
                    andi r16, 0x0F  ; zero out MSNibble
                    ori r16, 0x30   ; this adds '0'
                    rcall Lcd8BitData
                    pop r16
                    andi r16, 0x0F  ; zero out MSNibble
                    ori r16, 0x30   ; this adds '0'
                    rcall Lcd8BitData
                    ret

LcdBinaryOut:       push r17
                    push r16
                    ldi r17, 8
LcdBinaryOutNext:   pop r16
                    sbrs r16, 7
                    jmp LcdBinOutZero
                    lsl r16
                    push r16
                    ldi r16, '1'
                    rcall Lcd8BitData
                    rjmp LcdBinaryOutX
LcdBinOutZero:      lsl r16
                    push r16 
                    ldi r16, '0'
                    rcall Lcd8BitData
LcdBinaryOutX:      dec r17
                    brne LcdBinaryOutNext
                    pop r16
                    pop r17
                    ret


; ----------------------------------------------------------------
;   SPI
;
SPIInit:            sbi DDRB, SPI_SS
                    sbi DDRB, SPI_MOSI
                    sbi DDRB, SPI_SCK
                    sbi PORTB, SPI_SS ; no chip selected
                    ldi r16, (1<<SPE) | (1<<MSTR) | (1<<CPHA) | (1<<CPOL) | (1<<SPR1) | (1<<SPR0)
                    out SPCR, r16
                    ret

SPITransceive:      ; transmit byte in  r16
                    ; client code is responsible for setting SS low!
                    out SPDR, r16
SPISendWait:
                    in r0, SPSR
                    sbrs r0, SPIF
                    rjmp SPISendWait
                    in r16, SPDR
                    ret

SPIReadByte:        ; address is given in r16, value returned in r16
                    cbi PORTB, SPI_SS
                    rcall SPITransceive
                    ldi r16, 0x00
                    rcall SPITransceive
                    sbi PORTB, SPI_SS ; no chip selected
                    ret

SPIWriteByte:        ; address is given in r16, value in r17
                    ori r16, 0x80  ; set bit 7 to mark command
                    cbi PORTB, SPI_SS
                    rcall SPITransceive
                    mov r16, r17
                    rcall SPITransceive
                    sbi PORTB, SPI_SS ; no chip selected
                    ret



; --------------------------- MAIN ------------------------------
Reset:
                    ;; set up return stack
                    ldi r16, LOW(RAMEND)
                    out SPL, r16
                    ldi r16, HIGH(RAMEND)
                    out SPH, r16

                    sei

                    rcall LcdInit
                    rcall LcdClearScreen

                    ; just some init message, not really needed
                    ; only for show
                    ldi ZH, HIGH(2*MsgLcd)
                    ldi ZL, LOW(2*MsgLcd)
                    rcall LcdString
                    rcall LongWait
                    rcall LcdClearScreen

                    rcall SPIInit

                    ;; if you do not want to set the clock
                    rjmp Again
                    ;; set clock ---------------
                    ; day
                    ldi r16, 0x04
                    ldi r17, 0x19
                    rcall SPIWriteByte
                    ; month
                    ldi r16, 0x05
                    ldi r17, 0x04
                    rcall SPIWriteByte

                    ; year
                    ldi r16, 0x06
                    ldi r17, 0x15
                    rcall SPIWriteByte

                    ;; seconds
                    ldi r16, 0x00
                    ldi r17, 0x00
                    rcall SPIWriteByte

                    ;; minutes
                    ldi r16, 0x01
                    ldi r17, 0x40
                    rcall SPIWriteByte

                    ;; minutes
                    ldi r16, 0x02
                    ldi r17, 0x17
                    rcall SPIWriteByte

Again:   ; -------------------  this is the real clock display code ------------

                    rcall LcdHome

                    ldi r16, 0x02
                    rcall SPIReadByte
                    sbrc r16, 6 ; if this is clear, we have 24 mode, easy
                    ; we have 12 hour  mode -> clear bit 6 and bit 5
                    andi r16, 0x1f
                    rcall LCDBCDOut

                    ldi r16, ':'
                    rcall LCD8BitData

                    ldi r16, 0x01
                    rcall SPIReadByte
                    rcall LCDBCDOut

                    ldi r16, ':'
                    rcall LCD8BitData

                    ldi r16, 0x00
                    rcall SPIReadByte
                    rcall LCDBCDOut

                    ldi r16, 0x84
                    rcall LCDSetXY

                    ldi r16, 0x04
                    rcall SPIReadByte
                    rcall LCDBCDOut

                    ldi r16, '.'
                    rcall LCD8BitData

                    ldi r16, 0x05
                    rcall SPIReadByte
                    andi r16, 0x7F
                    rcall LCDBCDOut

                    ldi r16, '.'
                    rcall LCD8BitData

                    ldi r16, 0x06
                    rcall SPIReadByte
                    rcall LCDBCDOut

                    rjmp Again



End:
                    rjmp End
 

MsgLcd:     .db "LCD Init OK", 0, 0, 0
MsgSpi:     .db "SPI Init OK", 0, 0, 0

.dseg ;------------  RAM  -------------------------------------

.org SRAM_START
